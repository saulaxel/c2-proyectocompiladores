/*
 * File:   FinalCode.cpp
 * Author: ulises
 *
 * Created on 6 de julio de 2021, 19:36
 */

#include <exception>

#include "FinalCode.hpp"

namespace C2 {
FinalCode::FinalCode() {
}

FinalCode::FinalCode(const FinalCode& orig) {
    (void)orig;
    // No parece que este constructor sirva de algo pero no esto seguro.
    // Voy a dejar esta excepción para que me alerte de si es usado
    throw logic_error("El constructor de copia de FinalCode ha sido invocado");
}

FinalCode::~FinalCode() {
}

FinalCode::FinalCode(string name, vector<Quad> ci) {
    this->ci = ci;
    file.open(name);
}

void FinalCode::translate() {
    if (file.is_open()) {
        for (Quad code : ci) {
            processQuad(code);
        }
    }
    file.close();
}

void FinalCode::processQuad(Quad q) {
    string arg1, arg2, inst, res;
    arg1 = q.getArg1();
    arg2 = q.getArg2();
    res = q.getRes();
    inst = q.getOp();

    if (inst == "+") {
        file << "ADD " << arg1 << ", " << arg2 << endl;
        file << "MOVE .A, " << res << endl;
    }
    else if (inst == "-") {
        file << "SUB " << arg1 << ", " << arg2 << endl;
        file << "MOVE .A, " << res << endl;
    }
    else if (inst == "*") {
        file << "MUL " << arg1 << ", " << arg2 << endl;
        file << "MOVE .A, " << res << endl;
    }
    else if (inst == "/") {
        file << "DIV " << arg1 << ", " << arg2 << endl;
        file << "MOVE .A, " << res << endl;
    } else if (inst == "||") {
        file << "OR " << arg1 << ", " << arg2 << endl;
        file << "MOVE .A, " << res << endl;
    } else if (inst == "&&") {
        file << "AND " << arg1 << ", " << arg2 << endl;
        file << "MOVE .A, " << res << endl;
    } else if (inst == ">") {
        file << "CMP " << arg2 << ", " << arg1 << endl;
        file << "BN $5" << endl;
        file << "MOVE #0, " << res << endl;
        file << "BR $3" << endl;
        file << "MOVE #1," << res << endl;
    } else if (inst == "<") {
        file << "CMP " << arg1 << ", " << arg2 << endl;
        file << "BN $5" << endl;
        file << "MOVE #0, " << res << endl;
        file << "BR $3" << endl;
        file << "MOVE #1," << res << endl;
    } else if (inst == "==") {
        file << "CMP " << arg1 << ", " << arg2 << endl;
        file << "BZ $5" << endl;
        file << "MOVE #0, " << res << endl;
        file << "BR $3" << endl;
        file << "MOVE #1," << res << endl;
    } else if (inst == "!=") {
        file << "CMP " << arg1 << ", " << arg2 << endl;
        file << "BN $5" << endl;
        file << "MOVE #1, " << res << endl;
        file << "BR $3" << endl;
        file << "MOVE #0," << res << endl;
    } else if (inst == "label") {
        file << res << ":";
    } else if (inst == "ifFalse") {
        file << "CMP #0, " << arg1 << endl;
        file << "BZ " << res << endl;
    } else if (inst == "goto") {
        file << "BR " << res << endl;
    } else if (inst == "printw") {
        file << "WRINT " << arg1 << endl;
    } else if (inst == "prints") {
        file << "WRSTR " << arg1 << endl;
    } else if (inst == "string") {
        file << arg1 << ": DATA" << res << endl;
    } else if (inst == "end") {
        file << "HALT" << endl;
    }

}

} // namespace C2
