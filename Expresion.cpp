#include "Expresion.hpp"

namespace C2 {

Expresion::Expresion() {
    dir = "";
    refTipo = {.tipoId = -1, .tabla = nullptr};
}

Expresion::Expresion(string dir, RefTipo tipo) {
    this->dir = dir;
    this->refTipo = tipo;
}

Expresion::~Expresion() {

}

const RefTipo& Expresion::getRefTipo() const {
    return refTipo;
}

const string& Expresion::getDir() const {
    return dir;
}

void Expresion::setRefTipo(RefTipo tipo) {
    this->refTipo = tipo;
}

void Expresion::setDir(string dir) {
    this->dir = dir;
}

} /* Namespace C2 */
