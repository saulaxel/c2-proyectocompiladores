#include "PilaCount.hpp"

PilaCount::PilaCount() {

}

PilaCount::~PilaCount() {

}

void PilaCount::push(int i) {
    pila.push(i);
}

int PilaCount::pop() {
    int i = pila.top();
    pila.pop();
    return i;
}

int PilaCount::top() const {
    return pila.top();
}
