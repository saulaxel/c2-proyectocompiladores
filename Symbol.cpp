#define NO_INCLUDE_TABLE_HPP
#include "Symbol.hpp"

namespace C2 {

Symbol::Symbol() {

}

Symbol::Symbol(string id) {
    this->id = id;
    this->dir = -1;
}

Symbol::~Symbol() {

}

void Symbol::setDir(int dir) {
    this->dir = dir;
}

void Symbol::setRefTipo(RefTipo it) {
    this->refTipo = it;
}

void Symbol::setTypeVar(string tipo) {
    typeVar = tipo;
}

void Symbol::setParams(vector<int> params) {
    this->params = params;
}

const string& Symbol::getId() const {
    return id;
}

int Symbol::getDir() const {
    return dir;
}

const RefTipo& Symbol::getRefTipo() const {
    return refTipo;
}

const string& Symbol::getTypeVar() const {
    return typeVar;
}

const vector<int>& Symbol::getParams() const {
    return params;
}

} // namespace C2

// Como RefTipo requiere Table, este se incluye manualmente al final
#include  "Table.hpp"
