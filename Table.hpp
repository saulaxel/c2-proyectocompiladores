#ifndef TABLA_H
#define TABLA_H
#include <vector>

using namespace std;
namespace C2 { class Table; }
// Table se declara en un momento pero Type requerirá el conocimiento de la
// misma antes de que se declare
#include "RefTipo.hpp"
#include "Symbol.hpp"
#include "Type.hpp"


namespace C2 {
class Table {
    private:
        // Símbolos y tipos definidos en la tabla
        vector<Symbol> symTab;
        vector<Type> typeTab;

        vector<Symbol>::iterator searchSymbol(const string& id);
        Symbol& getSymbolFromId(const string& id);


    public:
        Table();
        ~Table();

        //Funciones para la tabla de símbolos
        void addSymbol(string id);
        bool isInSymbol(string id);
        void setDir(string id, int dir);
        void setRefTipo(string id, RefTipo tipo);
        void setTypeVar(string id, string tipo);
        void setParams(string id, vector<int> args);

        int getDir(const string& id);
        RefTipo getRefTipo(const string& id);
        const string& getTypeVar(const string& id);
        const vector<int>& getParams(const string& id);
        const vector<Symbol>& getSymTab();

        //Funciones para la tabla de tipos
        // Se debe modificar la función addType para que devuelva un entero
        // Este entero es el indice donde se agrego el último elemento
        int addType(string  name);
        int addType(string nombre, int tam);
        int addType(string nombre, RefTipo tipoBase, int numeroItems, int tamBase);

        string getName(int id);
        int getNumItems(int id);
        int getTam(int id);
        RefTipo getTipoBase(int id);
        int idTipoArray(int idBase, int numElem);

        Table& getBaseTable(int id);
        void setBaseTable(int id, Table* t);


        const vector<Type>& getTypes();
        const vector<Symbol>& getSyms();
        void freeMemory();

        // Usar la tabla como un tipo en sí (estructuras)
        int tamBytes;
        Table * outerTable;

};
} // namespace C2
#endif
