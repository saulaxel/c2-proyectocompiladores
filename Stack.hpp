#ifndef TSTACK_H
#define TSTACK_H

#include <stack>
#include "Table.hpp"
using namespace std;

namespace C2 {
class Stack {
    private:
        Table* global;
        std::stack<Table> pila;

    public:
        Stack();
        ~Stack();
        void push();
        Table pop();
        Table& top();
        Table& getGlobal();
};
} // namespace C2
#endif
