// Se define NO_INCLUDE_TABLE_HPP para evitar el conflicto de dependencia
// circular
#define NO_INCLUDE_TABLE_HPP
#include "Type.hpp"
#include "Symbol.hpp"

namespace C2 {

Type::Type(string name, RefTipo tipoBase, int tamBytes, int numItems) :
    name(name),
    idBaseType(tipoBase),
    tamBytes(tamBytes),
    numItems(numItems),
    baseTable(nullptr)
{
}

Type::Type(string name, int bytes) :
    name(name),
    idBaseType(RefTipo{-1, nullptr}),
    tamBytes(bytes),
    numItems(1),
    baseTable(nullptr)
{
}

Type::~Type() {
    // No se libera la memoria baseTable de forma intencional
    // Esto es porque varias copias de este objeto pueden compartir el
    // puntero y cada una intentará hacer delete, causando un error
    //
    // En su lugar, allá donde tengamos almacenada la última copia (lo cual
    // probablemente será tras el pop de una tabla) se debe mandar a llamar
    // manualmente a deleteBaseTable a través de Table::freeMemory().
    // TODO: Saber como afecta este destructor a las copias de tabla
}

RefTipo Type::getIdBaseType() {
    return idBaseType;
}

int Type::getNumItems() {
    return numItems;
}

int Type::getTamBytes() {
    return tamBytes;
}

string Type::getName() {
    return name;
}



Table& Type::getBaseTable() {
    return *this->baseTable;
}

void Type::setBaseTable(Table * baseTable) {
    this->baseTable = baseTable;
    this->numItems = 1;
    this->tamBytes = 0;

    if (baseTable != nullptr) {
        for (Symbol s : baseTable->getSyms()) {
            if (s.getTypeVar() != "struct") {
                RefTipo t = s.getRefTipo();
                tamBytes += t.getTam();
            }
        }
    }

}

void Type::deleteBaseTable() {
    // Ver nota en la implementación del destructor de esta clase
    if (baseTable != nullptr) {
        delete baseTable;
    }
    baseTable = nullptr;
}


} // namespace C2

#include "Table.hpp"
