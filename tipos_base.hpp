#ifndef DEFINICIONES_HPP
#define DEFINICIONES_HPP

namespace C2 {

const int TIPO_VOID = 0;
const int TIPO_CHAR = 1;
const int TIPO_INT = 2;
const int TIPO_FLOAT = 3;
const int TIPO_DOUBLE = 4;

const int TIPO_ERRONEO = -1;
const int TIPO_A_DETERMINAR = -2;


static inline bool esNumerico(int tipoId) {
    switch (tipoId) {
        case TIPO_INT: // Fallthrough
        case TIPO_FLOAT:
        case TIPO_DOUBLE:
            return true;
    }
    return false;
}

static inline string tipoEnCadena(int tipoId) {
    switch (tipoId) {
        case TIPO_VOID  : return "void"  ; break;
        case TIPO_CHAR  : return "char"  ; break;
        case TIPO_INT   : return "int"   ; break;
        case TIPO_FLOAT : return "float" ; break;
        case TIPO_DOUBLE: return "double"; break;
    }

    throw domain_error("Esta función solo presenta la representación en cadena de tipos base");
}

} // namespace C2
#endif // DEFINICIONES_HPP
