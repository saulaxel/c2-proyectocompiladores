#ifndef TYPE_H
#define TYPE_H
#include <string>
#include <memory>

#include "RefTipo.hpp"
// Type se define en un momento, pero Table requiere saber de él antes
namespace C2 { class Type; }
#include "Table.hpp"

using namespace std;

namespace C2 {
class Type {
    private:
        string name;
        RefTipo idBaseType;
        int tamBytes;
        int numItems;
        Table* baseTable;

    public:
        Type(string name, RefTipo tipoBase, int tamBytes, int numItems);
        Type(string name, int bytes);
        ~Type();

        RefTipo getIdBaseType();
        int getNumItems();
        int getTamBytes();
        string getName();
        Table& getBaseTable();
        void setBaseTable(Table* baseTable);
        void deleteBaseTable();
};
} // namespace C2
#endif
