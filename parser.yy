%skeleton "lalr1.cc"
%require  "3.0"
%debug
%defines
%define api.namespace {C2}
%define parser_class_name {Parser}

%code requires{

#include "Expresion.hpp"
#include "tipos_base.hpp"

#include <stack>
#include <vector>
#include <memory>

namespace C2 {
    class Driver;
    class Scanner;
}

// The following definitions is missing when %locations isn't used
# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif
}

%parse-param { Scanner  &scanner  }
%parse-param { Driver  &driver  }

%code{
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <algorithm>
#include <cctype>

using namespace std;

#include "Driver.hpp"

extern int yylineno;
extern int numType;

#undef yylex
#define yylex scanner.yylex
}

%define api.value.type variant
%define parse.assert

%token <std::string>    ID
%token <std::string>    NUMERO
%token <std::string>    STRING
%token                  IF SWITCH CASE WHILE DEFAULT FOR PRINT BREAK FUNC
%token                  INT VOID CHAR FLOAT DOUBLE RETURN STRUCT
%token                  LKEY RKEY PYC COMA DOSP
%token <std::string>    LITERAL_CHAR

%left                   ASIG /*=*/
%left                   OR /*||*/
%left                   AND /*&&*/
%left                   EQUAL DISTINCT /*== !=*/
%left                   LT GT  LTE GTE /* < >*/
%left                   MAS MENOS /* + -*/
%left                   MUL DIV MOD/* * / */
%left                   LCOR RCOR
%left                   NOT
%left                   DOT
%nonassoc               LPAR RPAR /* ( ) */
%nonassoc               IFX
%nonassoc               ELSE

%locations
%start programa
%type<int> base
%type<int> comp_arr
%type<RefTipo> tipo comp_struc
%type<std::vector<int>> lista_args args lista_params params
%type<int> type_param param arg
%type<C2::Expresion> expresion condicion sentReturn
%type<C2::Expresion> complemento arreglo left_part
%type<std::stack<string>> parte_array

%%
programa
    :
    {
        driver.init();
        driver._goto("main");
    }
    declaraciones
    {
        if (! driver.ocurrioError) {
            driver.printSymTab();
            driver.generarCodigoIntermedio();
            driver.translate();
        }
        // Todo: etiqueta main condicional
        // TOdo: Imprimir tab de símbolos tras cada función
    }
    ;

declaraciones
    :
    declaraciones
    declaracion
    |
    %empty
    ;

declaracion
    :
    tipo
    {
        driver.gType = $1;
    }
    decl_fun_var
    |
    decl_struct
    ;

decl_fun_var
    :
    lista_var PYC
    |
    decl_fun
    ;

lista_var
    :
    lista_var COMA var
    |
    var
    ;

var
    :
    ID
    {
        driver.variable($1);
    }
    ;

tipo
    :
    base
    {
        driver.gBase = $1;
    }
    comp_arr
    {
        RefTipo tipo = {.tipoId = $3, .tabla = &driver.getGlobal()};
        $$ = tipo;
    }
    |
    STRUCT ID
    {
        $$ = driver.tipoStruct($2);
    }
    ;

comp_arr
    :
    LCOR NUMERO RCOR comp_arr
    {
        $$ = driver.definirArreglo($4, $2);
    }
    |
    %empty
    {
        $$ = driver.gBase;
    }
    ;

base
    :
    CHAR
    {
        $$ = TIPO_CHAR;
    }
    |
    INT
    {
        $$ = TIPO_INT;
    }
    |
    FLOAT
    {
        $$ = TIPO_FLOAT;
    }
    |
    DOUBLE
    {
        $$ = TIPO_DOUBLE;
    }
    |
    VOID
    {
        $$ = TIPO_VOID;
    }
    ;

decl_struct
    :
    STRUCT LKEY
    {
        Table* outer = &driver.top();
        driver.push();
        driver.top().outerTable = outer;
        driver.pilaDir.push(driver.gDir);
        driver.gDir = 0;
    }
    body_struct RKEY
    {
        driver.gDir = driver.pilaDir.pop();
        Table* ptrTabla = new Table();
        *ptrTabla = driver.pop();

        // TODO: Pasar el tamaño de la estructura
        int tipoId = driver.top().addType("struct");
        driver.top().setBaseTable(tipoId, ptrTabla);
        driver.gType = {.tipoId = tipoId, .tabla = &driver.top()};
    }
    lista_var PYC
    |
    STRUCT ID LKEY
    {
        Table* outer = &driver.top();

        driver.push();
        driver.top().outerTable = outer;

        driver.pilaDir.push(driver.gDir);
        driver.gDir = 0;

    }
    body_struct RKEY
    {
        driver.gDir = driver.pilaDir.pop();
        Table* ptrTabla = new Table();
        *ptrTabla = driver.pop();

        int tipoId = driver.top().addType("struct");
        driver.gType = {.tipoId = tipoId, .tabla = &driver.top()};
        driver.top().setBaseTable(tipoId, ptrTabla);
        driver.estructura($2, driver.gType);
    }
    lista_var PYC
    ;

body_struct
    :
    body_struct decl_local
    |
    decl_local
    ;

decl_fun
    :
    ID LPAR
    {
        driver.push();
        driver.gListaRetorno = vector<int>();
    }
    lista_params
    {
        int tipoId = driver.gType.tipoId;
        if (driver.esArreglo(tipoId)) {
            driver.error("El retorno de la función no puede ser arreglo");
        } else {
            driver.definirFuncion($1, tipoId, $4);
        }
    }
    RPAR LKEY decl_locales bloqueSentencias RKEY
    {
        Table t = driver.pop();
        t.freeMemory(); // Ver nota en el destructor de la clase Type
    }
    ;

lista_params
    :
    params
    {
        $$ = $1;
    }
    |
    %empty
    {
        $$ = vector<int>();
    }
    ;

params
    :
    params COMA param
    {
        $$ = $1;
        $$.push_back($3);
    }
    |
    param
    {
        $$ = vector<int>{ $1 };
    }
    ;

param
    :
    type_param ID
    {
        $$ = $1;
        driver.parametro($2, $1);
    }
    ;

type_param
    :
    base
    {
        $$ = $1;
    }
    |
    base
    {
        driver.gBase = $1;
    }
    parte_array
    {
        std::stack<string>& parte_array = $3;
        int tipoId = driver.gBase;

        while (! parte_array.empty()) {
            bool permitirNegativo = (parte_array.size() == 1);
            tipoId = driver.definirArreglo(tipoId, parte_array.top(),
                                           permitirNegativo);
            if (tipoId == -1) {
                break;
            }

            parte_array.pop();
        }

        $$ = tipoId;
    }
    |
    STRUCT ID
    {
        $$ = driver.tipoStruct($2).tipoId;
    }
    ;

parte_array
    :
    parte_array LCOR NUMERO RCOR
    {
        $1.push($3);
        $$ = $1;
    }
    |
    LCOR RCOR
    {
        $$.push("-1");
    }
    ;

decl_locales
    :
    decl_locales
    decl_local
    |
    %empty
    ;

decl_local
    :
    decl_var
    |
    decl_struct
    ;

decl_var
    :
    tipo
    {
        driver.gType = $1;
    }
    lista_var PYC
    ;

bloqueSentencias
    :
    sentencias
    |
    %empty
    ;

sentencias
    :
    sentencias sentencia
    |
    sentencia
    ;

sentencia
    :
    sentIf
    |
    sentSwitch
    |
    sentWhile
    |
    sentFor
    |
    sentAsig
    |
    sentPutw
    |
    sentPuts
    |
    sentBreak
    |
    sentProc
    |
    sentReturn
    ;

sentReturn
    :
    RETURN expresion PYC
    {
        int tipoId = $2.getRefTipo().tipoId;
        Expresion returnExp;

        if (driver.esArreglo(tipoId)) {
            driver.error("Se intentó regresar un arreglo");
        } else if (tipoId != driver.gType.tipoId) {

            if (driver.convertibles(tipoId, driver.gType.tipoId)) {
                returnExp = driver.convertir($2, driver.gType.tipoId);
            } else {
                driver.error("Return con tipo no compatible");
            }

        } else {
            returnExp = $2;
        }
        driver.gListaRetorno.push_back(tipoId);
        driver.addQuad(Quad("return", returnExp.getDir(), "", ""));
    }
    |
    RETURN PYC
    {
        if (TIPO_VOID != driver.gType.tipoId) {
            driver.error("Se intentó regresar un valor en función void");
        }

        driver.gListaRetorno.push_back(TIPO_VOID);
        driver.addQuad(Quad("return", "", "", ""));
    }
    ;

sentProc
    :
    ID LPAR
    {
        driver.gLineasArgs = vector<int>();
        driver.gListaArgs = vector<Expresion>();
    }
    lista_args RPAR PYC
    {
        driver.llamarFuncion($1, $4, false);
    }
    ;

lista_args
    :
    args
    {
        $$ = $1;
    }
    |
    %empty
    {
        $$ = vector<int>();
    }
    ;

args
    :
    args COMA arg
    {
        $$ = $1;
        $$.push_back($3);
    }
    |
    arg
    {
        $$ = vector<int> { $1 };
    }
    ;

arg
    :
    expresion
    {
        $$ = $1.getRefTipo().tipoId;
        driver.gListaArgs.push_back($1);
        driver.gLineasArgs.push_back(scanner.lineno());
    }
    ;

expresion
    :
    expresion MAS expresion
    {
        $$ = driver.add($1, $3);
    }
    |
    expresion MENOS expresion
    {
        $$ = driver.sub($1, $3);
    }
    |
    expresion MUL expresion
    {
        $$ = driver.mul($1, $3);
    }
    |
    expresion DIV expresion
    {
        $$ = driver.mul($1, $3);
    }
    |
    expresion MOD expresion
    {
        // Todo, revisar tipos enteros
        $$ = driver.mod($1, $3);
        // Todo, cambiar dir a addr
    }
    |
    LPAR expresion RPAR
    {
        $$ = $2;
    }
    |
    NUMERO
    {
        $$ = driver.numero($1, numType);
    }
    |
    LITERAL_CHAR
    {
        $$ = Expresion($1, {.tipoId = TIPO_CHAR, .tabla = &driver.getGlobal()} );
    }
    |
    ID
    {
        driver.gId = $1;
    }
    complemento
    {
        $$ = $3;
    }
    ;

complemento
    :
    comp_struc
    {
        if ($1.tipoId != TIPO_ERRONEO) {
            if (driver.gId != driver.qualifiedIdentifier) {
                string dir = driver.gId + "[" + to_string(driver.gDes) + "]";
                $$ = Expresion(dir, $1);
            } else {
                $$ = Expresion(driver.gId, $1);
            }
        } else {
            $$ = Expresion();
        }
    }
    |
    arreglo
    {
        $$ = $1;
    }
    |
    LPAR lista_args RPAR
    {
        $$ = driver.llamarFuncion(driver.gId, $2, true);
    }
    ;

arreglo
    :
    arreglo LCOR expresion RCOR
    {
        $$ = driver.arreglo($1, $3);
    }
    |
    LCOR expresion RCOR
    {
        $$ = driver.arreglo($2);
    }
    ;

condicion
    :
    condicion OR condicion
    {
        $$ = driver._or($1, $3);
    }
    |
    condicion AND condicion
    {
        $$ = driver._and($1, $3);
    }
    |
    expresion EQUAL expresion
    {
        $$ = driver.equal($1, $3);
    }
    |
    expresion DISTINCT expresion
    {
        $$ = driver.distinct($1, $3);
    }
    |
    expresion GT expresion
    {
        $$ = driver.gt($1, $3);
    }
    |
    expresion LT expresion
    {
        $$ = driver.lt($1, $3);
    }
    |
    expresion GTE expresion
    {
        $$ = driver.gte($1, $3);
    }
    |
    expresion LTE expresion
    {
        $$ = driver.lte($1, $3);
    }
    |
    NOT condicion
    {
        $$ = driver.neg($2);
    }
    |
    LPAR condicion RPAR
    {
        $$ = $2;
    }
    ;

sentIf
    :
    {
        driver.numIf ++;
        driver.pilaIf.push(driver.numIf);
    }
    IF
    LPAR
    condicion
    {
        driver.ifFalse($4, "else", driver.pilaIf.top());
    }
    RPAR
    bloqueOSentencia
    {
        driver._goto("finIf", driver.pilaIf.top());
        driver.etiqueta("else", driver.pilaIf.top());
    }
    sentElse
    {
        driver.etiqueta("finIf", driver.pilaIf.top());
        driver.pilaIf.pop();
    }
    ;

sentElse
    :
    ELSE bloqueOSentencia
    |
    %empty %prec IFX
    ;

sentWhile
    :
    WHILE LPAR
    {
        driver.numWhile ++;
        driver.pilaWhile.push(driver.numWhile);
        driver.pilaSentencias.push({TipoSentencia::WHILE, driver.numWhile});
        driver.etiqueta("inicioWhile", driver.pilaWhile.top());
    }
    condicion
    {
        driver.ifFalse($4, "finWhile", driver.pilaWhile.top());
    }
    RPAR
    bloqueOSentencia
    {
        driver._goto("inicioWhile", driver.pilaWhile.top());
        driver.etiqueta("finWhile", driver.pilaWhile.top());
        driver.pilaWhile.pop();
        driver.pilaSentencias.pop();
    }
    ;

sentSwitch
    :
    SWITCH LPAR expresion
    {
        driver.gExpresionSwitch = $3;
        if (driver.gExpresionSwitch.getRefTipo().tipoId != TIPO_INT) {
            driver.error("La expresión del switch debe ser de tipo int");
        }
        driver.numSwitch ++;
        driver.pilaSwitch.push(driver.numSwitch);
        driver.pilaSentencias.push({TipoSentencia::SWITCH, driver.numSwitch});
        driver.pilaCondCase.push(vector<Quad>());
        driver._goto("cases", driver.pilaSwitch.top());
    }
    RPAR LKEY body_switch
    RKEY
    {
        driver._goto("finSwitch", driver.pilaSwitch.top());

        driver.etiqueta("cases", driver.pilaSwitch.top());
        driver.generarCases();

        driver.etiqueta("finSwitch", driver.pilaSwitch.top());
        driver.pilaSwitch.pop();
        driver.pilaSentencias.pop();
        driver.pilaCondCase.pop();
    }
    ;

body_switch
    :
    caso body_switch
    |
    predeterminado
    |
    %empty
    ;

caso
    :
    {
        driver.numCase ++;
        driver.pilaCase.push(driver.numCase);
    }
    CASE expresion
    {
        if ($3.getRefTipo().tipoId != TIPO_INT) {
            driver.error("La expresión del case debe ser de tipo int");
        }
        driver.condicionCase();
        Expresion condicion = driver.equal(driver.gExpresionSwitch, $3);
        driver._if(condicion, "case", driver.pilaCase.top());
        driver.finCondicionCase();
    }
    DOSP
    {
        driver.etiqueta("case", driver.pilaCase.top());
    }
    sentencias
    {
        driver.pilaCase.pop();
    }
    ;

predeterminado
    :
    {
        driver.numCase ++;
        driver.pilaCase.push(driver.numCase);
    }
    DEFAULT DOSP
    {
        driver.etiqueta("default", driver.pilaCase.top());
    }
    sentencias
    {
        driver.condicionCase();
        driver._goto("default", driver.pilaCase.top());
        driver.finCondicionCase();
        driver.pilaCase.pop();
    }
    ;

sentFor
    :
    FOR LPAR sentAsig
    {
        driver.numFor ++;
        driver.pilaFor.push(driver.numFor);

        driver.pilaSentencias.push({TipoSentencia::FOR, driver.numFor});
        driver.etiqueta("inicioFor", driver.pilaFor.top());
    }
    condicion
    {
        driver.ifFalse($5, "finFor", driver.pilaFor.top());
    }
    PYC
    {
        driver.incrementoFor();
    }
    sentAsig
    {
        // NOTA: SentAsig requiere dos puntos
        driver.finIncrementoFor();
    }
    RPAR
    bloqueOSentencia
    {
        driver.generarCodigoIncremento();
        driver._goto("inicioFor", driver.pilaFor.top());
        driver.etiqueta("finFor", driver.pilaFor.top());

        driver.pilaFor.pop();
        driver.pilaSentencias.pop();
    }
    ;

sentAsig
    :
    left_part
    ASIG expresion PYC
    {
        driver.asign($1, $3);
    }
    ;

left_part
    :
    ID
    {
        driver.gId = $1;
    }
    arreglo
    {
        $$ = $3;
    }
    |
    ID
    {
        driver.gId = $1;
        driver.gDes = 0;
    }
    comp_struc
    {
        if ($3.tipoId != TIPO_ERRONEO) {
            if (driver.gId != driver.qualifiedIdentifier) {
                string dir = driver.gId + "[" + to_string(driver.gDes) + "]";
                $$ = Expresion(dir, $3);
            } else {
                $$ = Expresion(driver.gId, $3);
            }
        } else {
            $$ = Expresion();
        }
    }
    ;

comp_struc
    :
    comp_struc DOT ID
    {
        if ($1.tipoId != TIPO_ERRONEO) {
            driver.qualifiedIdentifier += "." + $3;
            $$ = driver.accesoMiembro($1, $3);
        }
    }
    |
    %empty
    {
        // Todo: Regresar iden y containerTable en un solo tipo
        Expresion iden = driver.identificador(driver.gId); // Todo, comprobar retorno correcto

        driver.gDes = 0;
        driver.qualifiedIdentifier = driver.gId;

        if (iden.getRefTipo().tipoId != TIPO_ERRONEO) {
            driver.containerTable = iden.getRefTipo().tabla;
        }

        $$ = iden.getRefTipo();
    }
    ;

sentPutw
    :
    PRINT LPAR expresion
    RPAR PYC
    {
        driver.printExpresion($3);
    }
    ;

sentPuts
    :
    PRINT LPAR STRING
    RPAR PYC
    {
        driver.printString($3);
    }
    ;

sentBreak
    :
    BREAK
    PYC
    {
        driver.breakSentence();
    }
    ;

bloqueOSentencia
    :
    LKEY bloqueSentencias RKEY
    |
    sentencia
    ;

%%

void C2::Parser::error( const location_type &l, const std::string &err_message )
{
    cerr << "Error sintáctico: " << err_message << " en la ubicación " << l << endl;
}
