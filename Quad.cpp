#include "Quad.hpp"

namespace C2 {

Quad::Quad() {

}

Quad::Quad(string operador, string arg1, string arg2, string res) {
    this->op = operador;
    this->arg1 = arg1;
    this->arg2 = arg2;
    this->res = res;
}

const string& Quad::getOp() const {
    return op;
}

const string& Quad::getArg1() const {
    return arg1;
}

const string& Quad::getArg2() const {
    return arg2;
}

const string& Quad::getRes() const {
    return res;
}

} // namespace C2

#ifdef TEST_STANDALONE
#include <cassert>
#include <iostream>
using namespace std;

using C2::Quad;

Quad fun(Quad q) {
    return {q.getOp(), q.getArg1(), q.getArg2(), q.getRes()};
}

class A {
    void addQuad(Quad q);
    void prueba();
};

void A::addQuad(Quad q) {
    (void)q;
}

void A::prueba() {
    addQuad(Quad("a", "b", "c", "d"));
}

int main()
{
    C2::Quad q = {"a", "b", "c", "d"};

    assert(q.getOp() == "a");
    assert(q.getArg1() == "b");
    assert(q.getArg2() == "c");
    assert(q.getRes() == "d");

    q = {.operador = "e", .arg1 = "f", .arg1 = "g", .res = "h"};
    assert(q.getOp() == "e");
    assert(q.getArg1() == "f");
    assert(q.getArg2() == "g");
    assert(q.getRes() == "h");

    q = fun({"i", "j", "k", "l"});
    assert(q.getOp() == "i");
    assert(q.getArg1() == "j");
    assert(q.getArg2() == "k");
    assert(q.getRes() == "l");
}

#endif // TEST_STANDALONE
