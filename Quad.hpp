#ifndef CUADRUPLA_H
#define CUADRUPLA_H

#include <string>
using namespace std;

namespace C2 {
class Quad {
    private:
        string op;
        string arg1;
        string arg2;
        string res;

    public:
        Quad();
        Quad(string op, string arg1, string arg2, string res);
        const string& getOp() const;
        const string& getArg1() const;
        const string& getArg2() const;
        const string& getRes() const;

};
} // namespace C2
#endif
