#ifndef SYMBOL_H
#define SYMBOL_H
#include <string>
#include <vector>

// Se incluye RefTipo pero evitando que incluya Table
#include "RefTipo.hpp"

using namespace std;

namespace C2 {

class Symbol {
    private:
        string id;
        int dir;
        RefTipo refTipo;
        string typeVar;
        vector<int> params;

    public:
        Symbol();
        Symbol(string id);
        ~Symbol();
        void setDir(int dir);
        void setRefTipo(RefTipo tipo);
        void setTypeVar(string tipo);
        void setParams(vector<int> params);

        const string& getId() const;
        int getDir() const;
        const RefTipo& getRefTipo() const;
        const string& getTypeVar() const;
        const vector<int>& getParams() const;
};
} // namespace C2

#endif
