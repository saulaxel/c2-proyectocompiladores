%{
#include <string>
#include <iostream>
using namespace std;
bool coment = false;

#include "tipos_base.hpp"
int numType = 0;

#include "Scanner.hpp"
#undef  YY_DECL
#define YY_DECL int C2::Scanner::yylex( C2::Parser::semantic_type * const lval, C2::Parser::location_type * loc )

using token = C2::Parser::token;


#define YY_NO_UNISTD_H
#define YY_USER_ACTION loc->step(); loc->columns(yyleng);

%}

%option debug
%option nodefault
%option yyclass="C2::Scanner"
%option c++
%option yylineno

cadena \"([\x20-\x21\x23-\xFE])*\"
literal_char \'([\x20-\x21\x23-\xFE])?\'
entero [+-]?[0-9]+
numero_decimales [+-]?[0-9]+"."[0-9]*|[+-]?"."[0-9]+
exponente [eE]{entero}
doble {numero_decimales}{exponente}?|{entero}{exponente}

%s COMENTARIO

%%

%{          /** Code executed at the beginning of yylex **/
            yylval = lval;
%}
<INITIAL>{
{entero}    {
                numType = TIPO_INT;
                yylval->build< std::string >( yytext );
                return( token::NUMERO );
            }

{doble}     {
                numType = TIPO_DOUBLE;
                yylval->build< std::string >( yytext );
                return( token::NUMERO );
            }

{doble}"f"  {
                numType = TIPO_FLOAT;
                yylval->build< std::string >( yytext );
                return( token::NUMERO );
            }


{literal_char}  {
                    yylval->build< std::string >( yytext );
                    return( token::LITERAL_CHAR );
                }


"if"        {
                return( token::IF );
            }


"else"      {
                return( token::ELSE );
            }

"switch"    {
                return( token::SWITCH );
            }

"case"      {
                return( token::CASE );
            }

"default"   {
                return( token::DEFAULT );
            }

"for"       {
                return( token::FOR );
            }


"while"     {
                return( token::WHILE );
            }


"print"     {
                return( token::PRINT );
            }

"int"       {
                return( token::INT );
            }

"char"      {
                return( token::CHAR );
            }

"float"     {
                return( token::FLOAT );
            }

"double"    {
                return( token::DOUBLE );
            }

"void"      {
               return( token::VOID );
            }


"break"     {
               return( token::BREAK );
            }

"struct"    {
               return( token::STRUCT );
            }

"func"      {
               return( token::FUNC );
            }

"return"    {
               return( token::RETURN );
            }

[a-zA-Z][a-zA-Z0-9_]*   {
                            yylval->build< std::string >( yytext );
                            return( token::ID );
                        }

{cadena}    {
               yylval->build< std::string >( yytext );
               return( token::STRING );
            }
"("         {
               return( token::LPAR );
            }

")"         {
               return( token::RPAR );
            }

"{"         {
               return( token::LKEY );
            }

"}"         {
               return( token::RKEY );
            }

"["         {
               return( token::LCOR );
            }

"]"         {
               return( token::RCOR );
            }

":"         {
                return( token::DOSP );
            }

";"         {
               return( token::PYC );
            }

","         {
               return( token::COMA );
            }

"."         {
                return( token::DOT );
            }

"+"         {
               return( token::MAS );
            }

"-"         {
               return( token::MENOS );
            }

"*"         {
               return( token::MUL );
            }

"/"         {
               return( token::DIV );
            }

"%"         {
               return( token::MOD );
            }

">"         {
               return( token::GT );
            }

"<"         {
               return( token::LT );
            }

">="        {
               return( token::GTE );
            }

"<="        {
               return( token::LTE );
            }

"="         {
               return( token::ASIG );
            }

"=="        {
               return( token::EQUAL );
            }

"!="        {
               return( token::DISTINCT );
            }

"||"        {
               return( token::OR );
            }

"&&"        {
               return( token::AND );
            }

"!"         {
               return( token::NOT );
            }

"/*"     {
            coment = true;
            BEGIN(COMENTARIO);
         }

\n          {
               /** Update line number **/
               loc->lines();
            }

[ \t\r]     {}

.           {
               cout <<"ERROR LÉXICO: "<< yytext << endl;
            }
}
<COMENTARIO>{
[*/]  {}
[^*/] {}
"*/"  {
         coment=false;
         BEGIN(INITIAL);
      }
}
%%
int yyFlexLexer::yywrap() {
   return 1;
}
