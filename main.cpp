#include <iostream>
#include <cstdlib>
#include <cstring>

#include "Driver.hpp"

int main(const int argc, const char *argv[])
{
    C2::Driver driver;
    /** check for the right # of arguments **/
    if (argc == 2)
    {
        driver.parse(argv[1]);
    }
    else
    {
        driver.parse("prueba");
    }
    return (EXIT_SUCCESS);
}
