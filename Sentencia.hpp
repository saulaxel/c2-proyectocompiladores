#ifndef CICLO_HPP
#define CICLO_HPP
namespace C2 {

enum class TipoSentencia {
    WHILE,
    FOR,
    SWITCH
};

struct Sentencia {
    TipoSentencia tipo;
    int num;
};

} // namespace C2
#endif // CICLO_HPP
