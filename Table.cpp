#include "Table.hpp"
#include <iostream>
using std::cout;
#include <string>
using std::string;
#include <algorithm>
using std::find_if;

#include <stdexcept>
using std::out_of_range;

namespace C2 {

Table::Table() {
    // El orden en que añadimos los tipos importa. Tiene que ser el mismo
    // orden en que se declaran las constantes en tipos_base.hpp
    addType("void", 0);
    addType("char", 1);
    addType("int", 4);
    addType("float", 4);
    addType("double", 8);

    outerTable = nullptr;
}

Table::~Table() {

}

void Table::addSymbol(string id) {
    symTab.push_back(Symbol(id));
}

int Table::addType(string  name) {
    typeTab.push_back(Type(name, 4));
    return (int)typeTab.size() - 1;
}

int Table::addType(string name, int tam) {
    typeTab.push_back(Type(name, tam));
    return (int)typeTab.size() - 1;
}

int Table::addType(string name, RefTipo tipoBase, int numeroItems, int tamBase) {
    typeTab.push_back(Type(name, tipoBase, tamBase * numeroItems, numeroItems));
    return (int)typeTab.size() - 1;
}

bool Table::isInSymbol(string id) {
    return searchSymbol(id) != symTab.end();
}

void Table::setDir(string id, int dir) {
    getSymbolFromId(id).setDir(dir);
}

void Table::setRefTipo(string id, RefTipo refTipo) {
    getSymbolFromId(id).setRefTipo(refTipo);
}

void Table::setTypeVar(string id, string tipo) {
    getSymbolFromId(id).setTypeVar(tipo);
}

void Table::setParams(string id, vector<int> args) {
    return getSymbolFromId(id).setParams(args);
}

int Table::getDir(const string& id) {
    return getSymbolFromId(id).getDir();
}

RefTipo Table::getRefTipo(const string& id) {
    auto iterSymbol = searchSymbol(id);
    if (iterSymbol != symTab.end()) {
        // En esta función, a diferencia de las de alrededor, es válido
        // si no se encuentra el símbolo. Eso permite que esta función se use
        // para consultar si ya está declarado un identificador
        return (*iterSymbol).getRefTipo();
    }
    return {.tipoId = -1, .tabla = nullptr};
}

const string& Table::getTypeVar(const string& id) {
    return getSymbolFromId(id).getTypeVar();
}

const vector<int>& Table::getParams(const string& id) {
    return getSymbolFromId(id).getParams();
}

const vector<Symbol>& Table::getSymTab() {
    return symTab;
}

string Table::getName(int id) {
    return typeTab[id].getName();
}

int Table::getNumItems(int id) {
    return typeTab[id].getNumItems();
}

int Table::getTam(int id) {
    return typeTab[id].getTamBytes();
}

RefTipo Table::getTipoBase(int id) {
    return typeTab[id].getIdBaseType();
}

int Table::idTipoArray(int idBase, int numElem) {
    auto iterType = find_if(typeTab.begin(), typeTab.end(),
            [idBase, numElem](Type& t) {
                return t.getIdBaseType().tipoId == idBase &&
                       t.getNumItems() == numElem;
            }
    );

    if (iterType != typeTab.end()) {
        return std::distance(typeTab.begin(), iterType);
    }

    return -1;
}

Table& Table::getBaseTable(int id) {
    return typeTab[id].getBaseTable();
}

void Table::setBaseTable(int id, Table* t) {
    typeTab[id].setBaseTable(t);
}


const vector<Type>& Table::getTypes() {
    return typeTab;
}

const vector<Symbol>& Table::getSyms() {
    return symTab;
}

void Table::freeMemory() {
    for (Type s : typeTab) {
        s.deleteBaseTable();
    }
}

/**********************/
/* Métodos auxiliares */
/**********************/
vector<Symbol>::iterator Table::searchSymbol(const string& id) {
    return find_if(symTab.begin(), symTab.end(), [id](const Symbol& s) {
            return s.getId() == id;
    });
}

Symbol& Table::getSymbolFromId(const string& id) {
    auto iterSymbol = searchSymbol(id);
    if (iterSymbol != symTab.end()) {
        return (*iterSymbol);
    }
    throw out_of_range("Se trató de operar con un identificador inexistente: " + id);
}


} // namespace C2
