#ifndef PILA_COUNT_H
#define PILA_COUNT_H
#include <stack>
using namespace std;

class PilaCount {
    private:
        stack<int> pila;

    public:
        PilaCount();
        ~PilaCount();
        void push(int n);
        int pop();
        int top() const;
};

#endif
