#ifndef EXPRESION_H
#define EXPRESION_H
#include <string>

#include "RefTipo.hpp"
using namespace std;

namespace C2 {
class Expresion {
private:
    string dir;
    RefTipo refTipo;

public:
    Expresion();
    Expresion(string dir, RefTipo tipo);
    ~Expresion();

    const RefTipo& getRefTipo() const;
    const string& getDir() const;

    void setRefTipo(RefTipo tipo);
    void setDir(string dir);
};
} // namespace C2


#endif
