CC    ?= clang
CXX   ?= clang++

EXE = compiler

CDEBUG = -g -Wall -Wextra
CXXDEBUG = -g -Wall -Wextra

CSTD = -std=c11
CXXSTD = -std=c++11

CFLAGS = -Wno-deprecated-register -I$(PWD) -I$(PWD)/output_files -O0 $(CDEBUG) $(CSTD)
CXXFLAGS = -Wno-deprecated-register -I$(PWD) -I$(PWD)/output_files -O0 -O0  $(CXXDEBUG) $(CXXSTD)

CPPOBJ = main Driver Expresion FinalCode PilaCount  Quad Table Symbol Type Stack RefTipo IntermediateCode
SOBJ =  parser lexer

FILES = $(addsuffix .cpp, $(CPPOBJ))
OBJS  = $(addsuffix .o, $(CPPOBJ))
SOBJ := $(addsuffix .o, $(SOBJ))

OUTPUT_DIR = output_files

CLEANLIST =  $(addsuffix .o, $(OBJ)) $(OBJS) \
				 parser.tab.cc parser.tab.hh \
				 location.hh position.hh \
			    stack.hh parser.output parser.o \
				 lexer.o lexer.yy.cc $(EXE)\

CLEANLIST := $(addprefix $(OUTPUT_DIR)/, $(CLEANLIST))

SOBJ := $(addprefix $(OUTPUT_DIR)/, $(SOBJ))
OBJS := $(addprefix $(OUTPUT_DIR)/, $(OBJS))


.PHONY: all
all: $(OUTPUT_DIR)/$(EXE)

$(OUTPUT_DIR)/$(EXE): $(FILES) parser.yy lexer.l
	$(MAKE) $(SOBJ)
	$(MAKE) $(OBJS)
	$(CXX) $(CXXFLAGS) -o $@ $(OBJS) $(OUTPUT_DIR)/parser.o $(OUTPUT_DIR)/lexer.o $(LIBS)


$(OUTPUT_DIR)/parser.o: parser.yy tipos_base.hpp
	bison -d -v parser.yy -o $(OUTPUT_DIR)/parser.tab.cc
	$(CXX) $(CXXFLAGS) -c $(OUTPUT_DIR)/parser.tab.cc -o $@

$(OUTPUT_DIR)/lexer.o: lexer.l tipos_base.hpp
	flex --outfile=$(OUTPUT_DIR)/lexer.yy.cc $<
	$(CXX)  $(CXXFLAGS) -c $(OUTPUT_DIR)/lexer.yy.cc -o $@

# Compila main
$(OUTPUT_DIR)/%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

.PHONY: test
test:
	cd test && ./test0.pl

.PHONY: clean
clean:
	rm -rf $(CLEANLIST)
