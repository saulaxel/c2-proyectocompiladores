#ifndef __DRIVER_HPP__
#define __DRIVER_HPP__ 1

#include <string>
#include <cstddef>
#include <istream>
#include <vector>
#include <memory>
using namespace std;

#include "Scanner.hpp"
#include "parser.tab.hh"
#include "Stack.hpp"
#include "Expresion.hpp"
#include "Quad.hpp"
#include "PilaCount.hpp"
#include "tipos_base.hpp"
#include "RefTipo.hpp"
#include "Sentencia.hpp"

namespace C2 {
class Driver {
public:
   Driver() = default;
   virtual ~Driver();
   void parse( const string& filename );
   string file;
   bool trace_parsing;

   void init();

   /******************************************************************/
   /* AÑADIR O QUITAR TABLAS DE SÍMBOLOS EN EL STACK DE TABLAS */
   /******************************************************************/
   void push();
   Table& top();
   Table pop();
   Table& getGlobal();
   /*************************************************************************/
   /*     FUNCIONES QUE ENVUELVEN EL FUNCIONAMIENTO DE LA TABLA DE SÍMBOLOS */
   /*************************************************************************/
   bool globalIsInSymbol(string id);
   bool localIsInSymbol(string id);
   bool isInSymbol(string id);
   void addSymbol(string id);
   void setDir(string id, int dir);
   int getDir(string id);
   void setRefTipo(string id, RefTipo tipo);
   RefTipo getRefTipo(string id);
   void setVar(string id, string var);
   string getVar(string id);
   /*************************************************************************/
   /*     FUNCIONES QUE ENVUELVEN EL FUNCIONAMIENTO DE LA TABLA DE TIPOS */
   /*************************************************************************/
   string getName(int id);
   int getTam(int id);
   RefTipo getTipoBase(int id);
   int getNumItems(int id);
   const Table& getBaseTable(int id);
   void setBaseTable(int id, Table* baseTable);


   /****************************************************/
   /*     FUNCIONES QUE REALIZAN EL ANÁLISIS SEMÁNTICO */
   /****************************************************/
   bool esGlobal(const RefTipo& tipo);
   bool esArreglo(int tipo);
   bool arreglosCompatibles(int tipo1, int tipo2);

   bool convertibles(int tipoOrigen, int tipoDest);
   Expresion convertir(const Expresion &e, int tipoNuevo);
   Expresion operacionBinaria(const string&, const Expresion&, const Expresion&);
   Expresion add(const Expresion& e1, const Expresion& e2);
   Expresion sub(const Expresion& e1, const Expresion& e2);
   Expresion mul(const Expresion& e1, const Expresion& e2);
   Expresion div(const Expresion& e1, const Expresion& e2);
   Expresion mod(const Expresion& e1, const Expresion& e2);
   Expresion numero(const string& num, int tipo);
   Expresion ident(const string& id);
   Expresion _or(const Expresion& e1, const Expresion& e2);
   Expresion _and(const Expresion& e1, const Expresion& e2);
   Expresion gt(const Expresion& e1, const Expresion& e2);
   Expresion gte(const Expresion& e1, const Expresion& e2);
   Expresion lt(const Expresion& e1, const Expresion& e2);
   Expresion lte(const Expresion& e1, const Expresion& e2);
   Expresion equal(const Expresion& e1, const Expresion& e2);
   Expresion distinct(const Expresion& e1, const Expresion& e2);
   Expresion neg(const Expresion& e);

   Expresion arreglo(const Expresion& arreglo1, const Expresion& expresion);
   Expresion arreglo(const Expresion& expresion);

   int definirArreglo(int tipoBase, string numero, bool permitirNegativo = false);

   RefTipo accesoMiembro(const RefTipo& comp_struc, const string& id2);
   void asign(const Expresion& e1, const Expresion& e2);
   void declaracionVar(const string& clasificacion,
                      const string& id,
                      RefTipo tipo);
   void variable(const string& id, RefTipo tipo = {TIPO_A_DETERMINAR, nullptr});
   void parametro(const string& id, int tipoId);
   void estructura(const string& id, RefTipo tipo);

   void arg(const Expresion& e);
   void printExpresion(const Expresion& e);
   void printString(const string& s);

   RefTipo tipoStruct(const string& id);
   Expresion identificador(const string& id);

   void incrementoFor();
   void finIncrementoFor();
   void generarCodigoIncremento();

   void condicionCase();
   void finCondicionCase();
   void generarCases();

   void breakSentence(); // No me acepta el nombre break porque está reservado

   void definirFuncion(const string& id,
                       int tipoRetorno,
                       const vector<int>& listaParametros);
   Expresion llamarFuncion(const string& id,
                           const vector<int>& lista_args,
                           bool asignarResultado);


   /***********************************************/
   /*     FUNCIONES QUE GENERAN CÓDIGO INTERMEDIO */
   /***********************************************/
   void addQuadGlobalCode(Quad q);
   void addQuadCicloFor(Quad q);
   void addQuadCondCase(Quad q);

   void addQuad(Quad q);

   void _if(const Expresion& e, const string& label, int n);
   void ifFalse(const Expresion& e, const string& label, int n);
   void _goto(string label);
   void _goto(string label, int n);
   void etiqueta(string label, int n);
   string newLabel(int n);
   string newTemp();

   void printCI();

   void printSymTab();

   /*******************************************/
   /*     FUNCIONES QUE GENERAN CÓDIGO OBJETO */
   /*******************************************/
   int addString(string s);
   void generateStrings();
   void generarCodigoIntermedio();
   void translate();

   void error(const string& message);
   void error(const string& message, int lineno);
   void advertencia(const string& message);
   void advertencia(const string& message, int lineno);



private:
   Stack tstack;
   Parser  *parser  = nullptr;
   Scanner *scanner = nullptr;
   string output;
   string filename;

   void parse_helper( std::istream &stream );

public:

   std::stack<Sentencia> pilaSentencias;

   int numTemp;

   int numIf;
   PilaCount pilaIf;

   int numWhile;
   PilaCount pilaWhile;

   int numFor;
   PilaCount pilaFor;

   int numSwitch;
   PilaCount pilaSwitch;

   int numCase;
   PilaCount pilaCase;


   bool dentroIncrementoFor;
   bool condCase;
   bool existeDefault;

   bool declaracionLocal;

   bool ocurrioError;

   // Código intermedio de toda la aplicación
   vector<Quad> ci;
   // Código intermedio del incremento en el for
   // Si al usar stack no se especifica el espacio de nombres std,
   // el compilador usa C2::stack, que es una clase generada en stack.hh
   std::stack<vector<Quad>> pilaCodigoFor;
   std::stack<vector<Quad>> pilaCondCase;

   vector<string> strings;

   Table * containerTable;

   int gDir;
   string gId;
   RefTipo gType;
   int gBase;
   int gDes;
   string qualifiedIdentifier;
   Expresion gExpresionSwitch;
   vector<int> gListaRetorno;
   vector<Expresion> gListaArgs;
   vector<int> gLineasArgs; // TODO: No requieren ser global, gListaArgs, gLineasArgs
                            // y el tipo de retorno del arg pueden guardarse
                            // en una vector que contenga estructuras con esos
                            // tres datos
   PilaCount pilaDir;
};
} // namespace C2

#endif /* END __MCDRIVER_HPP__ */
