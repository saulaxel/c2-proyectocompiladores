#ifndef INTERMEDIATE_CODE_HPP
#define INTERMEDIATE_CODE_HPP
#include <fstream>
#include <string>
#include <vector>
#include <set>
using namespace std;

#include "Quad.hpp"

namespace C2 {
class IntermediateCode {
public:
    IntermediateCode(string name, vector<string> strings, vector<Quad> ci);
    ~IntermediateCode();
    void translate();
private:
    ofstream salida;
    vector<string> strings;
    vector<Quad> ci;

    void processQuad(Quad q);

    bool contains(set<string> conjunto, string cad);
};
} // namespace C2

#endif /* INTERMEDIATE_CODE_HPP */

