#include "Stack.hpp"

namespace C2 {

Stack::Stack() {
    pila.push(Table());
    global = &pila.top();
}

Stack::~Stack() {

}

void Stack::push() {
    pila.push(Table());
}

Table Stack::pop() {
    Table tab = pila.top();
    pila.pop();
    return tab;
}

Table& Stack::top() {
    return pila.top();
}

Table& Stack::getGlobal() {
    return *global;
}

} // namespace C2
