/*
 * File:   FinalCode.hpp
 * Author: ulises
 *
 * Created on 6 de julio de 2021, 19:36
 */

#ifndef FINALCODE_H
#define FINALCODE_H
#include <fstream>
#include <string>
#include <vector>
using namespace std;

#include "Quad.hpp"

namespace C2 {
class FinalCode {
public:
    FinalCode();
    FinalCode(string name, vector<Quad> ci);
    FinalCode(const FinalCode& orig);
    virtual ~FinalCode();
    void translate();
    void processQuad(Quad q);
private:
    ofstream file;
    vector<Quad> ci;

};
} // namespace C2

#endif /* FINALCODE_H */
