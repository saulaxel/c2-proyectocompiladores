#ifndef __SCANNER_HPP__
#define __SCANNER_HPP__ 1

#if ! defined(yyFlexLexerOnce)
#include "FlexLexer.h"
#endif

#include "parser.tab.hh"
#include "location.hh"

namespace C2 {

class Scanner : public yyFlexLexer {
public:

   Scanner(std::istream *in) : yyFlexLexer(in)
   {
      loc = new Parser::location_type();
   };

   //get rid of override virtual function warning
   using FlexLexer::yylex;

   virtual
   int yylex( Parser::semantic_type * const lval,
              Parser::location_type *location );


private:
   /* yyval ptr */
   Parser::semantic_type *yylval = nullptr;
   /* location ptr */
   Parser::location_type *loc    = nullptr;
};

} // namespace C2

#endif /* END __MCSCANNER_HPP__ */
