#include <cctype>
#include <fstream>
#include <cassert>
#include <cstdlib>
#include <sstream>
#include <exception>
#include <limits>
#include <cstdint>

#include "Driver.hpp"
#include "IntermediateCode.hpp"
#include "FinalCode.hpp"


namespace C2 {

/**********************************/
/* Inicialización y configuración */
/**********************************/
Driver::~Driver() {
    delete (scanner);
    scanner = nullptr;
    delete (parser);
    parser = nullptr;
}

void Driver::parse(const string& filename) {
    this->filename = filename;
    std::ifstream in_file(filename);
    if (!in_file.good()) {
        exit(EXIT_FAILURE);
    }
    parse_helper(in_file);
    return;
}

void Driver::parse_helper(std::istream &stream) {

    delete (scanner);
    try {
        scanner = new Scanner(&stream);

    } catch (std::bad_alloc &ba) {
        std::cerr << "Failed to allocate scanner: (" << ba.what() << "), exiting!!\n";
        exit(EXIT_FAILURE);
    }

    delete (parser);
    try {
        parser = new Parser((*scanner) /* scanner */,
                (*this) /* driver */);
    }    catch (std::bad_alloc &ba) {
        std::cerr << "Failed to allocate parser: (" << ba.what() << "), exiting!!\n";
        exit(EXIT_FAILURE);
    }

    const int accept(0);
    if (parser->parse() != accept) {
        std::cerr << "Parse failed!!\n";
    }
    return;
}

void Driver::init() {
    gDir = 0;

    pilaIf = PilaCount();
    numIf = 0;

    pilaWhile = PilaCount();
    numWhile = 0;

    pilaFor = PilaCount();
    numFor = 0;
    dentroIncrementoFor = false;
    condCase = false;

    pilaDir = PilaCount();

    numTemp = 0;
    gType = RefTipo({.tipoId = TIPO_ERRONEO, .tabla = nullptr});
    gBase = 0;

    ocurrioError = false;
    int pos =  filename.find(".");
    if (pos == -1) // No hay punto
    {
        pos = filename.length();
    }
    output = filename.substr(0, pos) + ".ens";
}

/******************************************************************/
/* AÑADIR O QUITAR TABLAS DE SÍMBOLOS EN EL STACK DE TABLAS */
/******************************************************************/
void Driver::push() {
    tstack.push();
}

Table& Driver::top() {
    return tstack.top();
}

Table Driver::pop() {
    Table t = tstack.pop();
    return t;
}

Table& Driver::getGlobal() {
    return tstack.getGlobal();
}


/*************************************************************************/
/*     FUNCIONES QUE ENVUELVEN EL FUNCIONAMIENTO DE LA TABLA DE SÍMBOLOS */
/*************************************************************************/
bool Driver::globalIsInSymbol(string id) {
    return tstack.getGlobal().isInSymbol(id);
}

bool Driver::localIsInSymbol(string id) {
    return tstack.top().isInSymbol(id);
}

bool Driver::isInSymbol(string id) {
    return localIsInSymbol(id) ||  globalIsInSymbol(id);
}

void Driver::addSymbol(string id) {
    tstack.top().addSymbol(id);
}

void Driver::setDir(string id, int dir) {
    tstack.top().setDir(id, dir);
}

int Driver::getDir(string id) {
    return tstack.top().getDir(id);
}

void Driver::setRefTipo(string id, RefTipo tipo) {
    tstack.top().setRefTipo(id, tipo);
}

RefTipo Driver::getRefTipo(string id) {
    return tstack.top().getRefTipo(id);
}

void Driver::setVar(string id, string var) {
    tstack.top().setTypeVar(id, var);
}

string Driver::getVar(string id) {
    return tstack.top().getTypeVar(id);
}

/*************************************************************************/
/*     FUNCIONES QUE ENVUELVEN EL FUNCIONAMIENTO DE LA TABLA DE TIPOS */
/*************************************************************************/
string Driver::getName(int id) {
    return tstack.top().getName(id);
}

int Driver::getTam(int id) {
    return tstack.top().getTam(id);
}

RefTipo Driver::getTipoBase(int id) {
    return tstack.top().getTipoBase(id);
}

int Driver::getNumItems(int id) {
    return tstack.top().getNumItems(id);
}

const Table& Driver::getBaseTable(int id) {
    return tstack.top().getBaseTable(id);
}

void Driver::setBaseTable(int id, Table* baseTable) {
    tstack.top().setBaseTable(id, baseTable);
}

/****************************************************/
/*     FUNCIONES QUE REALIZAN EL ANÁLISIS SEMÁNTICO */
/****************************************************/
bool Driver::esGlobal(const RefTipo& tipo) {
    return tipo.tabla == &getGlobal();
}

bool Driver::esArreglo(int tipo) {
    return getGlobal().getName(tipo) == "arreglo";
}

bool Driver::arreglosCompatibles(int tipo1, int tipo2) {
    if (! esArreglo(tipo1) || ! esArreglo(tipo2)) {
        throw invalid_argument("Esta función solo se puede llamar con tipos arreglo");
    }

    return getGlobal().getTipoBase(tipo1) == getGlobal().getTipoBase(tipo2);
}


bool Driver::convertibles(int tipoOrigen, int tipoDest) {
    return esNumerico(tipoOrigen) && esNumerico(tipoDest);
}

Expresion Driver::convertir(const Expresion& e, int tipoNuevo) {
    RefTipo tipoOriginal = e.getRefTipo();
    int idTipoOrig = tipoOriginal.tipoId;

    if (!esNumerico(idTipoOrig) || !esNumerico(idTipoOrig)) {
        throw invalid_argument(
              "Antes de llamar a esta función se debió comprobar "
              "que los tipos fueran numéricos. Hay un error de lógica");
    }

    if (idTipoOrig == tipoNuevo) {
        return e;
    }

    string strTipoOrig = tipoEnCadena(idTipoOrig);
    string strTipoDest = tipoEnCadena(tipoNuevo);

    if (idTipoOrig > tipoNuevo) {
        advertencia("Se está intentando hacer una conversión con pérdida de tipo "
                    + strTipoOrig + " a " + strTipoDest);
    }

    // Algunos warnings sobre perdida de precisión que se pueden calcular en
    // tiempo de compilación
    if (idTipoOrig == TIPO_INT && ! ::isalpha(e.getDir()[0]) &&
            tipoNuevo == TIPO_FLOAT) {

        long long i = stoll(e.getDir());

        if (i > 0x1p24 || i < -0x1p24) {
            advertencia("El entero " + e.getDir() + " no se puede convertir "
                        "a flotante de forma exacta. Se perderá precisión");
        }
    }

    string instruccion = strTipoOrig + "to" + strTipoDest;

    string temp = newTemp();

    addQuad(Quad(instruccion, e.getDir(), "", temp));
    return Expresion(temp, {.tipoId = tipoNuevo, .tabla = &getGlobal()});
}

Expresion Driver::operacionBinaria(const string& op,
                                   const Expresion& e1,
                                   const Expresion& e2) {
    int tipo1 = e1.getRefTipo().tipoId;
    int tipo2 = e2.getRefTipo().tipoId;

    if (tipo1 == TIPO_ERRONEO || tipo2 == TIPO_ERRONEO) {
        error("Se está intentando operar con expresiones inválidas");
        return Expresion();
    }


    if (convertibles(tipo1, tipo2)) {
        // Comparar el tamaño en bytes no es lo que queremos ya que,
        // por ejemplo, "float" e "int" tienen el mismo tamaño en bytes pero
        // "float" puede almacenar cantidades mayores. En su lugar se
        // está haciendo la comparación por id del tipo, lo cual resuelve el
        // problema haciendo que "float" tenga un valor que "int" pero también
        // tiene la desventaja de que depende de que los id que le
        // damos a cada tipo estén ordenados por capacidad del número.
        // Las definiciones de tipos base deben seguir lo establecido en
        // "tipos_base.hpp"
        int tipoResult = std::max(tipo1, tipo2);
        auto result = Expresion(newTemp(), {
                .tipoId = tipoResult,
                .tabla = &getGlobal()
        });

        Expresion a1 = convertir(e1, tipoResult);
        Expresion a2 = convertir(e2, tipoResult);

        addQuad(Quad(op, a1.getDir(), a2.getDir(), result.getDir()));

        return result;
    }

    error("Se está tratando de operar con tipos no numéricos");
    return Expresion();
}

Expresion Driver::add(const Expresion& e1, const Expresion& e2) {
    return operacionBinaria("+", e1, e2);
}

Expresion Driver::sub(const Expresion& e1, const Expresion& e2) {
    return operacionBinaria("-", e1, e2);
}

Expresion Driver::mul(const Expresion& e1, const Expresion& e2) {
    return operacionBinaria("*", e1, e2);
}

Expresion Driver::div(const Expresion& e1, const Expresion& e2) {
    return operacionBinaria("/", e1, e2);
}

Expresion Driver::mod(const Expresion& e1, const Expresion& e2) {
    return operacionBinaria("%", e1, e2);
}

Expresion Driver::numero(const string& num, int tipo) {
    RefTipo tipoNumero = {.tipoId = tipo, .tabla = &getGlobal()};
    if (tipo == TIPO_INT) {
        try {
            long long i = stoll(num);

            if (i > INT32_MAX) {
                advertencia("El valor " + num + " es mayor al límite de los enteros");

                return Expresion(to_string(INT32_MAX), tipoNumero);
            } else if (i < INT32_MIN) {
                advertencia("El valor " + num + " es menor al límite de los enteros");
                return Expresion(to_string(INT32_MIN), tipoNumero);
            }

        } catch (std::out_of_range&) {
            error("El entero " + num + " es demasiado grande para el compilador");
            return Expresion("0", tipoNumero);
        }
    } else if (tipo == TIPO_FLOAT) {
        try {
            long double d = stold(num);

            if (d > std::numeric_limits<float>::max()) {
                advertencia("El valor " + num + " es mayor al límite de los flotantes");
                return Expresion(to_string(std::numeric_limits<float>::max()), tipoNumero);
            } else if (d < std::numeric_limits<float>::lowest()) {
                advertencia("El valor " + num + " es menor al límite de los flotantes");
                return Expresion(to_string(std::numeric_limits<float>::lowest()), tipoNumero);
            }
        } catch (std::out_of_range&) {
            error("El número con decimales " + num + " es demasiado grande para el compilador");
            return Expresion("0.0f", tipoNumero);
        }
    } else if (tipo == TIPO_DOUBLE) {

        try {
            long double d = stold(num);

            if (d > std::numeric_limits<double>::max()) {
                advertencia("El valor " + num + " es mayor al límite de los dobles");
                return Expresion(to_string(std::numeric_limits<double>::max()), tipoNumero);
            } else if (d < std::numeric_limits<double>::lowest()) {
                advertencia("El valor " + num + " es menor al límite de los dobles");
                return Expresion(to_string(std::numeric_limits<double>::lowest()), tipoNumero);
            }

        } catch (std::out_of_range&) {
            error("El número con decimales " + num + " es demasiado grande para el compilador");
            return Expresion("0.0", tipoNumero);
        }
    }

    return Expresion(num, {.tipoId = tipo, .tabla = &getGlobal()});
}

Expresion Driver::ident(const string& id) {
    // DUDA: ¿Por qué esto está en dos líneas en lugar de decir
    // top().getRefTipo(id)?
    Table& table = tstack.top();

    RefTipo tipo = table.getRefTipo(id);
    if (tipo.tipoId == TIPO_ERRONEO) {
        tipo = getGlobal().getRefTipo(id);

        if (tipo.tipoId == TIPO_ERRONEO) {
            error("El identificador " + id + "no está declarado");
        }
    }

    Expresion e2(id, tipo);
    return e2;
}

Expresion Driver::_or(const Expresion& e1, const Expresion& e2) {
    return operacionBinaria("or", e1, e2);
}

Expresion Driver::_and(const Expresion& e1, const Expresion& e2) {
    return operacionBinaria("and", e1, e2);
}

Expresion Driver::gt(const Expresion& e1, const Expresion& e2) {
    return operacionBinaria(">", e1, e2);
}

Expresion Driver::gte(const Expresion& e1, const Expresion& e2) {
    return operacionBinaria(">=", e1, e2);
}

Expresion Driver::lt(const Expresion& e1, const Expresion& e2) {
    return operacionBinaria("<", e1, e2);
}

Expresion Driver::lte(const Expresion& e1, const Expresion& e2) {
    return operacionBinaria("<=", e1, e2);
}

Expresion Driver::equal(const Expresion& e1, const Expresion& e2) {
    return operacionBinaria("==", e1, e2);
}

Expresion Driver::distinct(const Expresion& e1, const Expresion& e2) {
    return operacionBinaria("!=", e1, e2);
}

Expresion Driver::neg(const Expresion& e) {
    auto res = Expresion(newTemp(), e.getRefTipo());
    addQuad(Quad("=", e.getDir(), "", res.getDir()));
    return res;
}

Expresion Driver::arreglo(const Expresion& arreglo1,
                          const Expresion& expresion) {

    if (expresion.getRefTipo().tipoId != TIPO_INT) {
        error("El índice de un arreglo debe ser entero y se encontró: " + expresion.getDir());
        return Expresion();
    }


    if (tstack.getGlobal().getName(arreglo1.getRefTipo().tipoId) == "arreglo") {
        int tipoId = arreglo1.getRefTipo().tipoId;
        RefTipo base = getGlobal().getTipoBase(tipoId);
        string dirNuevoArr = newTemp();
        int tamBase = getGlobal().getTam(base.tipoId);

        string temporal = newTemp();

        addQuad(Quad("*", expresion.getDir(), to_string(tamBase), temporal));
        addQuad(Quad("+", arreglo1.getDir(), temporal, dirNuevoArr));


        return Expresion(dirNuevoArr, base);
    }

    error("La variable " + gId + " es un arreglo con un numero menor de "
          "dimensiones");

    return Expresion();
}

Expresion Driver::arreglo(const Expresion& expresion) {
    Table * tabla = nullptr;

    if (localIsInSymbol(gId)) {
        tabla = &top();
    } else if (globalIsInSymbol(gId)) {
        tabla = &getGlobal();
    } else {
        error("El identificador " + gId + " no está declarado");
        return Expresion();
    }

    if (expresion.getRefTipo().tipoId != TIPO_INT) {
        error("El índice del arreglo debe ser entero");
        return Expresion();
    }

    int tipo = tabla->getRefTipo(gId).tipoId;

    if (getGlobal().getName(tipo) != "arreglo") {
        error(gId + " no es un arreglo");
        return Expresion();
    }

    RefTipo base = getGlobal().getTipoBase(tipo);
    string dir = newTemp();
    int baseTam = getGlobal().getTam(base.tipoId);

    addQuad(Quad("*", expresion.getDir(), to_string(baseTam), dir));

    return Expresion(dir, base);
}

int Driver::definirArreglo(int tipoBase, string numero, bool permitirNegativo) {

    bool esEntero = all_of(numero.begin(), numero.end(), [](char c) { return isdigit(c) || c == '+' || c == '-'; });
    if (! esEntero) {
        error("El índice de un arreglo debe ser entero y se encontró: " + numero);
        return -1;

    }

    int numItems = stoi(numero);
    if (! permitirNegativo && numItems < 0) {
        error("El índice debe ser positivo y se encontró: " + numero);
        return -1;
    }

    int idTipo = getGlobal().idTipoArray(tipoBase, numItems);

    if (idTipo != TIPO_ERRONEO) {
        return idTipo;
    }

    // Si el tipo arreglo con las mismas características no está
    // definido, lo definimos
    int tamBase = getGlobal().getTam(tipoBase);

    RefTipo base = {.tipoId = tipoBase, .tabla = &getGlobal()};
    idTipo = getGlobal().addType("arreglo", base, numItems, tamBase);

    return idTipo;
}

RefTipo Driver::accesoMiembro(const RefTipo& refTipoStruct,
                                const string& id2) {
    int tipoStruct = refTipoStruct.tipoId;
    // TODO: Eliminar containerTable


    if (containerTable->getName(tipoStruct) != "struct") {
        error("La variable " + qualifiedIdentifier
             + " no es de tipo estructura");
        return {TIPO_ERRONEO, nullptr};
    }


    Table& tabla = containerTable->getBaseTable(tipoStruct);

    if (! tabla.isInSymbol(id2)) {
        error("El campo " + id2 + " no existe en la estructura");
        return {TIPO_ERRONEO, nullptr};
    }

    // Nota: quizá dir se puede llamar storage
    gDes += tabla.getDir(id2);
    RefTipo tipoMiembro = tabla.getRefTipo(id2);

    containerTable = tipoMiembro.tabla;
    return tipoMiembro;
}

void Driver::asign(const Expresion& e1, const Expresion& e2) {
    if (e1.getRefTipo() == e2.getRefTipo()) {
        addQuad(Quad("=", e2.getDir(), "", e1.getDir()));

        return;
    }

    int tipoLeft = e1.getRefTipo().tipoId;
    int tipoRight = e2.getRefTipo().tipoId;

    if (esNumerico(tipoLeft) && esNumerico(tipoRight)) {
        Expresion a = convertir(e2, tipoLeft);

        addQuad(Quad("=", a.getDir(), "", e1.getDir()));

        return;
    }

    // La condición es aproximación para no repetir mensajes de error
    if (tipoLeft == TIPO_ERRONEO || tipoRight == TIPO_ERRONEO) {
        error("Se está intentando operar con expresiones inválidas");
    } else {
        error("Tipos no compatibles");
    }
}

void Driver::declaracionVar(const string& clasificacion,
                      const string& id,
                      RefTipo tipo) {

    if ( localIsInSymbol(id) ) {
        error("El identificador " + id + " ya fue usado en este contexto");
        return;
    }

    if (tipo.tipoId == TIPO_ERRONEO) {
        return;
    }

    if (tipo.tipoId == TIPO_VOID) {
        error("El tipo no puede ser void");
        return;
    }

    addSymbol(id);
    setRefTipo(id, tipo);
    setVar(id, clasificacion);

    if (clasificacion == "struct") {
        // En estructuras no establecemos dirección
        return;
    }

    setDir(id, gDir);

    gDir += tipo.getTam();

}

void Driver::variable(const string& id, RefTipo tipo) {
    if (tipo.tipoId == TIPO_A_DETERMINAR) {
        tipo = gType;
    }
    declaracionVar("variable", id, tipo);
}

void Driver::parametro(const string& id, int tipoId) {
    RefTipo tipo = {.tipoId = tipoId, .tabla = &getGlobal()};
    declaracionVar("parametro", id, tipo);
}

void Driver::estructura(const string& id, RefTipo tipo) {
    declaracionVar("struct", id, tipo);
}

void Driver::arg(const Expresion& e) {
    addQuad(Quad("arg", e.getDir(), "", ""));
}

void Driver::printExpresion(const Expresion& e) {
    addQuad(Quad("printw", e.getDir(), "", ""));
}

void Driver::printString(const string& s) {
    int stringId = addString(s);

    addQuad(Quad("prints", to_string(stringId), "", ""));
}

RefTipo Driver::tipoStruct(const string& id) {
    Table* t = &top();

    while (t != nullptr) {
        if (t->isInSymbol(id)) {

            if (t->getTypeVar(id) != "struct") {
                error(id + " no es de tipo estructura");
                return {TIPO_ERRONEO, nullptr};
            }

            return t->getRefTipo(id);
        }
        t = t->outerTable;
    }

    if (getGlobal().isInSymbol(id)) {
        if (getGlobal().getTypeVar(id) != "struct") {
            error(id + "no es de tipo estructura");
            return {TIPO_ERRONEO, nullptr};
        }
        return getGlobal().getRefTipo(id);
    }

    error("El tipo estructurado " + id + " no ha sido declarado");
    return {TIPO_ERRONEO, nullptr};
}

Expresion Driver::identificador(const string& id) {
    if (top().isInSymbol(id)) {

        RefTipo refTipo = top().getRefTipo(id);

        return Expresion(id, refTipo);
    }

    if (getGlobal().isInSymbol(id)) {

        RefTipo refTipo = getGlobal().getRefTipo(id);

        return Expresion(id, refTipo);
    }

    error("El id no se declaró");
    return Expresion(id, {.tipoId = TIPO_ERRONEO, .tabla = nullptr});
}

void Driver::incrementoFor() {
    dentroIncrementoFor = true;
    pilaCodigoFor.push(vector<Quad>());
}

void Driver::finIncrementoFor() {
    dentroIncrementoFor = false;
}

void Driver::generarCodigoIncremento() {
    for (Quad q: pilaCodigoFor.top()) {
        addQuadGlobalCode(q);
    }
    pilaCodigoFor.pop();
}

void Driver::condicionCase() {
    condCase = true;
}

void Driver::finCondicionCase() {
    condCase = false;
}

void Driver::generarCases() {
    for (Quad q: pilaCondCase.top()) {
        addQuadGlobalCode(q);
    }

}

void Driver::breakSentence() {
    if (pilaSentencias.empty()) {
        error("No se puede poner un break fuera de un ciclo");

        return;
    }

    Sentencia c = pilaSentencias.top();
    string etiqueta;
    if (c.tipo == TipoSentencia::WHILE) {
        etiqueta = "finWhile";
    } else if (c.tipo == TipoSentencia::FOR) {
        etiqueta = "finFor";
    } else if (c.tipo == TipoSentencia::SWITCH) {
        etiqueta = "finSwitch";
    } else {
        throw domain_error("Sentencia no contemplada para break");
    }

    etiqueta += to_string(c.num);

    addQuad(Quad("break", "", "", etiqueta));
}

void Driver::definirFuncion(const string& id,
                            int tipoRetorno,
                            const vector<int>& listaParametros) {

    if (getGlobal().isInSymbol(id)) {
        error("El id ya fue declarado");
        return;
    }

    getGlobal().addSymbol(id);
    getGlobal().setRefTipo(id, {.tipoId = tipoRetorno, .tabla = &getGlobal()});
    getGlobal().setTypeVar(id, "func");
    getGlobal().setParams(id, listaParametros);

    addQuad(Quad("label", "", "", id));
}

Expresion Driver::llamarFuncion(const string& id,
                                const vector<int>& lista_args,
                                bool asignarResultado)
{

    if (! tstack.getGlobal().isInSymbol(id)) {
        error("No existe el id de la función a invocar");
        return Expresion();
    }

    const string& var = tstack.getGlobal().getTypeVar(id);
    if ("func" != var) {
        error("El identificador " + id + "no hace referencia a una función");
        return Expresion();
    }

    const vector<int>& lista_params = tstack.getGlobal().getParams(id);
    int num_params = lista_params.size();

    if (num_params != (int)lista_args.size()) {
        error("El número de argumentos no coincide" );
        return Expresion();
    }

    for (int i = 0; i < num_params; i++) {
        if (lista_args[i] != lista_params[i]) {

            if (convertibles(lista_args[i], lista_params[i])) {
                Expresion a1 = convertir(gListaArgs[i], lista_params[i]);
                arg(a1);
            } else if (esArreglo(lista_args[i]) && esArreglo(lista_params[i])) {
                if (arreglosCompatibles(lista_args[i], lista_params[i])) {
                    arg(gListaArgs[i]);
                } else {
                    error("Arreglos no compatibles para el argumento " + to_string(i + 1));
                }
            } else {
                error("El tipo del argumento " + to_string(i + 1) + " no es correcto",
                        gLineasArgs[i]);
                return Expresion();
            }
        }
    }

    string dirResultado = "";
    if (asignarResultado) {
        dirResultado = newTemp();
    }

    addQuad(Quad("call", id, to_string(num_params), dirResultado));

    if (asignarResultado) {
        RefTipo tipo = getGlobal().getRefTipo(id);
        return Expresion(dirResultado, tipo);
    }

    return Expresion();
}


/***********************************************/
/*     FUNCIONES QUE GENERAN CÓDIGO INTERMEDIO */
/***********************************************/
void Driver::addQuadGlobalCode(Quad q) {
    ci.push_back(q);
}

void Driver::addQuadCicloFor(Quad q) {
    pilaCodigoFor.top().push_back(q);
}

void Driver::addQuadCondCase(Quad q) {
    pilaCondCase.top().push_back(q);
}

void Driver::addQuad(Quad q) {
    if (dentroIncrementoFor) {
        addQuadCicloFor(q);
    } else if (condCase) {
        addQuadCondCase(q);
    } else {
        addQuadGlobalCode(q);
    }
}

void Driver::_if(const Expresion& e,
                              const string& label,
                              int n)
{
    stringstream l;
    l << label << n;
    addQuad(Quad("if", e.getDir(), "", l.str()));

}
void Driver::ifFalse(const Expresion& e,
                              const string& label,
                              int n)
{
    stringstream l;
    l << label << n;
    addQuad(Quad("ifFalse", e.getDir(), "", l.str()));
}

void Driver::_goto(string label) {
    addQuad(Quad("goto", "", "", label));
}

void Driver::_goto(string label, int n) {
    stringstream l;
    l << label << n;
    addQuad(Quad("goto", "", "", l.str()));
}

void Driver::etiqueta(string label, int n) {
    stringstream l;
    l << label << n;
    addQuad(Quad("label", "", "", l.str()));
}

string Driver::newLabel(int n) {
    stringstream l;
    l << "L" << n;
    return l.str();
}

string Driver::newTemp() {
    stringstream l;
    l << "t" << numTemp;
    numTemp++;
    return l.str();
}

void Driver::printCI() {
    cout << "Código intermedio " << ci.size() << endl;
    for (Quad q : ci) {
        if (q.getOp() == "=")
            cout << "\t\t" << q.getRes() << " " << q.getOp() << " " << q.getArg1() << endl;
        else if (q.getOp() == "label")
            cout << q.getRes() << ": " << endl;
        else if (q.getOp() == "ifFalse")
            cout << "\t\t" << q.getOp() << " " << q.getArg1() << " goto " << q.getRes() << endl;
        else if (q.getOp() == "goto")
            cout << "\t\t" << q.getOp() << " " << q.getRes() << endl;
        else if (q.getArg2() == "")
            cout << "\t\t" << q.getRes() << " = " << q.getOp() << " " << q.getArg1() << endl;
        else
            cout << "\t\t" << q.getRes() << " = " << q.getArg1() << " " << q.getOp() << " " << q.getArg2() << endl;
    }
}

void Driver::printSymTab() {
    cout << "Tabla de símbolos" << endl;
    for (Symbol s : tstack.top().getSymTab()) {
        cout << s.getId() << "\t" << s.getDir() << "\t" << s.getRefTipo().tipoId << "\t" << s.getTypeVar() << endl;
    }
}

int Driver::addString(string s) {
    auto iter = find(strings.begin(), strings.end(), s);
    if (iter != strings.end()) {
        return distance(strings.begin(), iter);
    }

    strings.push_back(s);
    return strings.size() - 1;
}

void Driver::generateStrings() {
    for (string s : strings) {
        addQuad(Quad("string", s, "", ""));
    }
}

void Driver::generarCodigoIntermedio() {
    string archivo_inter = filename.substr(0, filename.length()) + ".inter";
    IntermediateCode ic(archivo_inter, strings, ci);
    ic.translate();
}

void Driver::translate() {
    FinalCode fc(output, ci);
    fc.translate();
}

void Driver::error(const string& message) {
    cerr << "Error semántico: " << message
         << " en la línea " << scanner->lineno()
         << endl;
    ocurrioError = true;
}

void Driver::error(const string& message, int lineno) {
    cerr << "Error semántico: " << message
         << " en la línea " << lineno
         << endl;
    ocurrioError = true;
}

void Driver::advertencia(const string& message) {
    cerr << "Advertencia: " << message
         << " en la línea " << scanner->lineno()
         << endl;
}

void Driver::advertencia(const string& message, int lineno) {
    cerr << "Advertencia: " << message
         << " en la línea " << lineno
         << endl;
}


} // Namespace C2
