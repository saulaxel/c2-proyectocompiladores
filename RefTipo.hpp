#ifndef REF_TIPO_HPP
#define REF_TIPO_HPP

namespace C2 {
    struct RefTipo;
    struct Table;
}

namespace C2 {

// RefTipo provee una forma conveniente de identificar
// un elemento en las tablas de tipos. Te indica en qué tabla
// se encuentra mediante un apuntador a la misma
//
// El tipoId es el índice del tipo en la tabla correspondiente

struct RefTipo {
    int tipoId;
    Table* tabla;

    // Por la comodidad de usar == entre dos objetos RefTipo
    bool operator==(const RefTipo& other) const {
        return tabla == other.tabla && tipoId == other.tipoId;
    }

    bool operator!=(const RefTipo& other) const {
        return !(*this == other);
    }

    int getTam();
};

} // namespace C2


// Por lo generar los contextos en los que se usa RefTipo ya cuentan tienen
// definida Table por lo que la siguiente línea no haría nada. No obstante, en
// algunos archivos esto  causa un conflicto de dependencia circular que
// resolvemos con el siguiente condicional. Aparte, se define la macro
// correspondiente donde se presentaba el conflicto
#  ifndef NO_INCLUDE_TABLE_HPP
#    include "Table.hpp"
#  endif

#endif // REF_TIPO_HPP
