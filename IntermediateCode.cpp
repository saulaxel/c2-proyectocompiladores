#include "IntermediateCode.hpp"

namespace C2 {

IntermediateCode::IntermediateCode(string name, vector<string> strings, vector<Quad> ci) {
    this->strings = strings;
    this->ci = ci;
    salida.open(name);
}

IntermediateCode::~IntermediateCode() {

}

void IntermediateCode::translate() {
    if (salida.is_open()) {
        for (std::size_t i = 0; i < strings.size(); i++) {
            salida << "DATA: string " << i << " :" << strings[i] << endl;
        }

        salida << '\n';

        for (Quad code : ci) {
            processQuad(code);
        }
    }
    salida.close();
}

bool IntermediateCode::contains(set<string> conjunto, string cad) {
    auto it = conjunto.find(cad);
    return it != conjunto.end();
}

void IntermediateCode::processQuad(Quad q) {
    static set<string> operadoresBinarios {
            +"+", "-", "*", "/", "%",
            "or", "and", ">", ">=", "<", "<=", "==", "!="
    };

    string arg1, arg2, inst, res;
    arg1 = q.getArg1();
    arg2 = q.getArg2();
    res = q.getRes();
    inst = q.getOp();

    if (contains(operadoresBinarios, inst)) {
        salida << res << " = " << arg1 << ' ' << inst << ' ' << arg2 << '\n';
    }
    else if (inst == "=") {
        salida << res << " = " << arg1 << '\n';
    }
    else if (inst == "label") {
        salida << res << ":" << '\n';
    }
    else if (inst == "goto" || "break") {
        salida << inst << res << '\n';
    }
    else if (inst == "if" || inst == "ifFalse") {
        salida << inst << arg1 << " goto " << res << '\n';
    }
    else if (inst == "prints" || inst == "printw") {
        salida << inst << arg1 << '\n';
    }
    else if (inst == "call") {
        salida << res << " = call" << ' ' << arg1 << ' ' << arg2 << '\n'
    }
    else if (inst == "arg" || inst == "return") {
        salida << inst << arg1 << '\n';
    }


    flush(salida);

}

} // namespace C2
